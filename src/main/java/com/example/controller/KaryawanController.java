package com.example.controller;

import java.util.List;
import javax.validation.Valid;
import com.example.exception.Exception;

import org.springframework.data.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.dao.KaryawanDao;
import com.example.model.Karyawan;

@RestController
public class KaryawanController {
	@Autowired
	private KaryawanDao karyawanDao;
	
	@GetMapping("/karyawan")
	public List<Karyawan> getAllData() {
        return karyawanDao.findAll();
    }
	
	@PostMapping("/karyawan")
	public Karyawan createData(@Valid @RequestBody Karyawan data) {
		return karyawanDao.save(data);
	}
	
	@PutMapping("/karyawan/{karyawanId}")
	public Karyawan updateData(@PathVariable Long karyawanId, @Valid @RequestBody Karyawan dataRequest) {
		return karyawanDao.findById(karyawanId).map(data ->{
			data.setAlamat(dataRequest.getAlamat());
			data.setTelp(dataRequest.getTelp());
			data.setGaji(dataRequest.getGaji());
			return karyawanDao.save(data);
		}).orElseThrow(()->new Exception("Data not found with id" + karyawanId));
	}
	
	@DeleteMapping("/karyawan/{karyawanId}")
    public ResponseEntity<?> deleteData(@PathVariable Long karyawanId) {
        return karyawanDao.findById(karyawanId).map(data -> {
            karyawanDao.delete(data);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new Exception("Data not found with id " + karyawanId));
    }
}
