package com.example.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Karyawan;


public interface KaryawanDao extends JpaRepository<Karyawan, Long> {
	
}
