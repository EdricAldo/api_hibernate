package com.example.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.DynamicInsert;

@Entity
@Table(name = "karyawan")
@DynamicInsert
public class Karyawan implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String nama;
	
	@NotNull
	private String alamat;
	
	@NotNull
	private String telp;
	
	private String emcontact;
	
	@NotNull
	private String gaji;
	
	@NotNull
	private String workfrom;
	
	@NotNull
	private String resign;
	
	@Column(name = "createdat", columnDefinition = "TIMESTAMP default NOW()")
	private Date createdat;
	
	
	private String createdby;
	
	@Column(name = "updatedat", columnDefinition = "TIMESTAMP default NOW()")
	private Date updatedat;
	
	
	private String updatedby;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getTelp() {
		return telp;
	}

	public void setTelp(String telp) {
		this.telp = telp;
	}

	public String getEmcontact() {
		return emcontact;
	}

	public void setEmcontact(String emcontact) {
		this.emcontact = emcontact;
	}

	public String getGaji() {
		return gaji;
	}

	public void setGaji(String gaji) {
		this.gaji = gaji;
	}

	public String getWorkfrom() {
		return workfrom;
	}

	public void setWorkfrom(String workfrom) {
		this.workfrom = workfrom;
	}

	public String getResign() {
		return resign;
	}

	public void setResign(String resign) {
		this.resign = resign;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	
}
